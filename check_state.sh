#!/bin/bash

state=$(set -x; awslocal kafka describe-cluster --cluster-arn $cluster_arn | jq -r .ClusterInfo.State)

for i in {1..35}; do
  echo "Waiting for Kafka cluster to become ACTIVE (current status: $state)..."
  sleep 4
  if [ "$state" == ACTIVE ]; then
    break
  elif [ "$state" == FAILED ]; then
    echo "Cluster creation FAILED, exiting..."
    exit 1
  fi
  state=$(awslocal kafka describe-cluster --cluster-arn $cluster_arn | jq -r .ClusterInfo.State)
done

if [ "$state" != ACTIVE  ]; then
  echo "Gave up waiting on Cluster, exiting..."
  exit 1
fi